#ifndef UTILS_H
#define UTILS_H

#include "image.h"


#include <stdio.h>



FILE* open_file(char* path, char* mode);
struct image* rotate_90d_counterclockwise(struct image* image);
#endif
