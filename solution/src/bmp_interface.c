#include "bmp_interface.h"

#include <stdlib.h>

struct bmp_header header;




enum write_status to_bmp(FILE* out, struct image *img){
  header.biWidth = img -> width;
  header.biHeight = img -> height;
  //writing header
  fwrite(&header, sizeof(struct bmp_header), 1, out);
  //padding
  uint32_t padding = (4 - (img->width * sizeof(struct pixel)) % 4) % 4;
  //writing rotated img to file

  for (uint32_t i = 0; i < img->height; i++) {
    fwrite(&(img->data[i * img->width]), sizeof(struct pixel), img->width, out);

    //add padding bytes
    for (int i = 0; i < padding; i++) {
      uint8_t paddingByte = 0;
      fwrite(&paddingByte, sizeof(uint8_t), 1, out);
    }

  }
  if(out){
    printf("Image successfully written to BMP file\n");

    free(img -> data);
    free(img);

    return WRITE_OK;
  } else {
    printf("Error while writing to BMP file\n");

    free(img -> data);
    free(img);
    
    return WRITE_ERROR;
  }
}




enum read_status from_bmp( FILE* in, struct image* img ){

    if(fread(&header, sizeof(struct bmp_header), 1, in)){
        //checking bmp signature
        if(header.bfType == 0x4D42){
            
            img -> width = header.biWidth;
            img -> height = header.biHeight;
            img -> data = (struct pixel*) malloc(header.biWidth * header.biHeight * (sizeof(struct pixel)));


            if(img->data){

              uint32_t padding = (4 - (img->width * sizeof(struct pixel)) % 4) % 4;


              for(uint32_t i = 0; i < img -> height; i++){
                //reading 1 row skipping padding
                fread(&(img -> data[i * img -> width]), sizeof(struct pixel), img -> width, in);
                fseek(in, padding, 1);

              }
              printf("BMP to internal type successful\n");
              return READ_OK;
            }
            free(img);
            printf("BMP file seems to be corrupted\n");
            return READ_INVALID_BITS;
        }
        printf("Is not a BMP file\n");
        return READ_INVALID_SIGNATURE;
    }
    return READ_INVALID_HEADER;
}
