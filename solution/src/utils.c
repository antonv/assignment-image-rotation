#include "utils.h"

#include <stdio.h>
#include <stdlib.h>

FILE* open_file(char* path, char* mode){
    FILE* file = fopen(path, mode);
    if(file){
        return file;
    } 
    printf("Error opening file: %s", path);
    return NULL;
    exit(1);
}


struct image* rotate_90d_counterclockwise(struct image* image){
    struct image* rotated_img = (struct image*) malloc(sizeof(struct image));
    if (!rotated_img) {
        printf("Memory allocation for rotated image failed\n");
        return 0;
    }
  rotated_img -> width = image -> height;
  rotated_img -> height = image -> width;
  rotated_img -> data = (struct pixel*) malloc(rotated_img -> width * rotated_img -> height * (sizeof(struct pixel)));

  if (!rotated_img->data) {
        printf("Memory allocation for rotated image data failed\n");
        free(rotated_img);
        return 0;
    }
  
  //rotating
  for(int32_t i = 0; i < image -> height; i++){
    for(int32_t j = 0; j < image -> width; j++){

      rotated_img -> data[j * rotated_img -> width + image -> height - 1 - i] = image -> data[i * image->width + j]; 
    }
  }
  printf("Rotation successfull\n");


  free(image->data);
  free(image); 

  return rotated_img;

}
