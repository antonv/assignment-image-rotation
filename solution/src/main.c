#include "bmp_interface.h"
#include "image.h"
#include "utils.h"

#include <stdio.h>
#include <stdlib.h>






int main( int argc, char** argv ) {
    if(argc == 3){
        struct image* img = (struct image*) malloc(sizeof(struct image));
            if (!img) {
                    printf("Memory allocation failed\n");
                    return 0;
        }
        FILE* file = open_file(argv[1], "rb");
            if (!file) {
                    free(img);
                    return 0;
        }
        from_bmp(file, img);
        fclose(file);
        struct image* rotated_img = rotate_90d_counterclockwise(img);

        to_bmp(open_file(argv[2], "wb"), rotated_img);

        return 0;
    }
    printf("not enough arguments");
    return 0;
}
